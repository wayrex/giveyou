﻿(function () {
  'use strict';

  angular
    .module('common')
    .constant('SIGNALR_PATH', setBaseUrl());

  function setBaseUrl() {
    var path = 'signalr';
    if (location.hostname !== 'localhost') {
      return location.origin + /GYServer/ + path;
    }
    return 'https://localhost:44300/' + path;
  }
})();