﻿(function () {
  angular
    .module('common.userInfo')
    .factory('userInfoService', userInfoService);

  userInfoService.$inject = ['storageService'];

  function userInfoService(storageService) {
    function saveUserFunction(user) {
      storageService.setItem('Identifier', user.UserId, false);
      storageService.setItem('FullName', user.FullName, false);
      storageService.setItem('EmailAddress', user.EmailAddress, false);
    }

    function getUserFunction() {
      var user = {
        UserId: storageService.getItem('Identifier', false),
        FullName: storageService.getItem('FullName', false),
        EmailAddress: storageService.getItem('EmailAddress', false)
      };

      return user;
    }

    function removeUserFunction() {
      storageService.removeItem('Identifier', false);
      storageService.removeItem('FullName', false);
      storageService.removeItem('EmailAddress', false);
    }

    function verifyExistsCategoryFunction(categoryId) {
      var userObject = angular.fromJson(storageService.getItem('user', true));
      if (userObject) {
        var userId = parseInt(getUserFunction().UserId, 10);
        var userPosition = getUserPosition(userObject, userId);

        if (userPosition > -1) {
          var categoryArray = userObject[userPosition][1];

          if (categoryArray) {
            for (var i = 0; i < categoryArray.length; i++) {
              if (categoryArray[i] === categoryId) {
                return false;
              }
            }
          }
        }
      }
      return true;
    }

    function storeCategoryInLocalStorageFunction(categoryId) {
      var userId = parseInt(getUserFunction().UserId, 10);
      var userArray = angular.fromJson(storageService.getItem('user', true));
      var categoriesList = [];
      var informationToBeStore = [];

      if (userArray !== null) {
        var userPosition = getUserPosition(userArray, userId);

        if (userPosition > -1) {
          var categoryArray = userArray[userPosition][1];
          var categoryPosition = getCategoryPosition(categoryArray, categoryId);

          if (categoryPosition < 0) {
            categoryArray.push(categoryId);
          } else {
            categoryArray.splice(categoryPosition, 1);
          }
        } else {
          categoriesList = [
            categoryId
          ];
          informationToBeStore = [
            userId,
            categoriesList
          ];

          userArray.push(informationToBeStore);
        }

        storageService.setItem('user', angular.toJson(userArray), true);
      } else {
        categoriesList = [
          categoryId
        ];

        informationToBeStore = [
          [
            userId,
            categoriesList
          ]
        ];

        storageService.setItem('user', angular.toJson(informationToBeStore), true);
      }
    }

    function getUserPosition(objectArray, userIdToFind) {
      for (var i = 0; i < objectArray.length; i++) {
        if (objectArray[i][0] === userIdToFind) {
          return i;
        }
      }

      return -1;
    }

    function getCategoryPosition(objectArray, categoryToFind) {
      for (var i = 0; i < objectArray.length; i++) {
        if (objectArray[i] === categoryToFind) {
          return i;
        }
      }

      return -1;
    }

    return {
      saveUser: saveUserFunction,
      getUser: getUserFunction,
      removeUser: removeUserFunction,
      storeCategoryInLocalStorage: storeCategoryInLocalStorageFunction,
      verifyExistsCategory: verifyExistsCategoryFunction
    };
  }
})();