﻿(function () {
  'use strict';

  angular
    .module('common')
    .constant('BASE_URL', setBaseUrl());

  function setBaseUrl() {
    if (location.hostname !== 'localhost') {
      return location.origin + '/GYServer/api';
    }
    return 'https://localhost:44300/api';
  }
})();