(function () {
  'use strict';

  angular
    .module('common')
    .constant('RESOURCES_URL', setBaseUrl());

  function setBaseUrl() {
    var path = 'uploads/images/';
    if (location.hostname !== 'localhost') {
      return location.origin + /GYServer/ + path;
    }
    return 'https://localhost:44300/' + path;
  }
})();