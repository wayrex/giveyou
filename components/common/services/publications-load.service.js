﻿(function () {
  'use strict';

  angular
    .module('common.services')
    .factory('publicationLoadService', publicationLoadService);

  publicationLoadService.$inject = ['publicationRestService', 'RESOURCES_URL'];

  function publicationLoadService(publicationRestService, RESOURCES_URL) {
    return {
      getPublications: getPublications
    };

    function getPublications(params) {
      return publicationRestService.getList(params)
        .then(function (response) {
          return buildPublications(response);
        })
        .catch(function (reason) {
          if (reason.status === 404) {
            return [];
          }
        });
    };

    function buildPublications(publications) {
      publications.forEach(function (publication) {
        if (publication.ImageName) {
          publication.ImageURL = RESOURCES_URL + publication.ImageName;
        }
      });
      return publications;
    };
  }
})();