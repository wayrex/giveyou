﻿(function () {
  'use strict';

  angular
    .module('common.services')
    .factory('authorizationService', authorizationService);

  authorizationService.$inject = ['$timeout', '$state', 'userInfoService'];

  function authorizationService($timeout, $state, userInfoService) {
    return {
      validateLogin: validateLogin
    };
    function validateLogin(toState) {
      if (angular.isUndefined(toState.needsLogin)) {
        toState.needsLogin = true;
      }
      var user = userInfoService.getUser();
      if (user.UserId === null && toState.needsLogin !== false) {
        $timeout(function () {
          $state.transitionTo('login');
        });
      }
    };
  }
})();