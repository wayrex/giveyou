﻿(function () {
  'use strict';
  angular
    .module('common.services')
    .factory('scrollService', scrollService);

  scrollService.$inject = ['$window', '$document'];

  function scrollService($window, $document) {

    function moveTo(idElement) {
      $window.scrollTo(0, $document.find(idElement).offsetTop);
    }

    return {
      moveTo: moveTo
    };
  }
})();