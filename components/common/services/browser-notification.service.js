(function () {
  'use strict';

  angular
    .module('common.services')
    .factory('browserNotificationService', browserNotificationService);

  browserNotificationService.$inject = ['$window'];
  function browserNotificationService($window) {
    var notificationTitle = 'GiveYou app';
    var notificationBody = '';

    var service = {
      createNotification: createNotification
    };

    return service;

    function createNotification(title, body) {
      if ($window.Notification) {
        if ($window.Notification.permission !== 'denied') {
          setNotificationParameters(title, body);
          $window.Notification.requestPermission(newNotification);
          return true;
        }
      }
      return false;
    }

    function setNotificationParameters(title, body) {
      if (title) {
        notificationTitle = title;
      }
      if (body) {
        notificationBody = body;
      }
    }

    function newNotification() {
      if ($window.Notification.permission === 'granted') {
        var options = {
          body: notificationBody
        };
        var notification = new $window.Notification(notificationTitle, options);
      }
    }
  }
})();