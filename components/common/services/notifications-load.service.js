﻿(function () {
  'use strict';

  angular
    .module('common.services')
    .factory('notificationLoadService', notificationLoadService);

  notificationLoadService.$inject = ['Restangular', 'RESOURCES_URL'];

  function notificationLoadService(Restangular, RESOURCES_URL) {
    return {
      getNotifications: getNotifications
    };

    function getNotifications(param) {
      var notificationsPromise = Restangular.one('users', param).getList('notifications');
      notificationsPromise.then(function (result) {
        return buildNotifications(result);
      });
      return notificationsPromise;
    };

    function buildNotifications(notifications) {
      notifications.forEach(function (notification) {
        notification.ImageURL = notification.ImageName ? RESOURCES_URL + notification.ImageName : '';
      });

      return notifications;
    };
  }
})();