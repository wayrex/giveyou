﻿(function () {
  'use strict';

  angular
    .module('common', [
      'common.services',
      'common.storage',
      'common.userInfo',
      'common.authentication'
    ]);
})();