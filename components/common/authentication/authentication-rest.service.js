﻿(function () {
  'use strict';

  angular
    .module('common.authentication')
    .factory('AuthenticationRestService', authenticationRestService);

  authenticationRestService.$inject = ['Restangular'];

  function authenticationRestService(Restangular) {
    return Restangular.service('login');
  }
})();