﻿(function () {
  angular
    .module('common.authentication')
    .factory('authService', authService);

  authService.$inject = [
    '$q',
    '$state',
    'AuthenticationRestService',
    'userInfoService'
  ];

  function authService($q, $state, AuthenticationRestService, userInfoService) {
    function loginFunction(username, password) {
      var credentials = {
        Username: username,
        Password: password
      };

      return AuthenticationRestService.post(credentials).then(
        function (response) {
          userInfoService.saveUser(response);
          return response.data;
        },
        function (error) {
          return $q.reject(error.data);
        }
      );
    }
    function logoutFunction() {
      userInfoService.removeUser();
      $state.go('login');
    }

    return {
      login: loginFunction,
      logout: logoutFunction
    };
  }
})();