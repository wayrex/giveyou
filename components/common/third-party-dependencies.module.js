﻿(function () {
  'use strict';

  angular
    .module('common.thirdPartyDependencies', [
      'restangular',
      'ui.router',
      'yaru22.angular-timeago'
    ]);
})();