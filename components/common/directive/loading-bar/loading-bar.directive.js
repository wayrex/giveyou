﻿(function () {
  'use strict';
  angular.module('common')
    .directive('loadingBar', function () {
      return {
        restrict: 'E',
        scope: {
          showLoadingBar: '='
        },
        templateUrl: 'components/common/directive/loading-bar/loading-bar.template.html'
      };
    });
})();