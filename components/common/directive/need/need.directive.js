﻿/* global $ */
(function () {
  'use strict';
  angular.module('common')
      .directive('need', function ($rootScope, needRestService) {
        return {
          restrict: 'E',
          scope: {
            publication: '=',
            currentUserId: '=',
            myNeeds: '='
          },
          templateUrl: 'components/common/directive/need/need.template.html',
          link: function (scope, element) {
            scope.visibleNeeds = false;
            scope.visibleComments = false;
            scope.showLoadingBar = false;
            scope.isRequested = (scope.publication.PublicationType === 'Requested') ? true : false;

            scope.showNeedButton = function () {
              var currentUser = scope.currentUserId;
              var publicationOwner = scope.publication.UserDto.UserId;

              var userHasComment = false;
              if (scope.publication.Requesters) {
                scope.publication.Requesters.forEach(function (need) {
                  if (need.UserId === currentUser) {
                    userHasComment = true;
                  }
                });
              }

              if(currentUser !== publicationOwner && !userHasComment) {
                return true;
              }
              return false;
            };

            scope.showNeedButton();

            scope.expandNeedInput = function () {
              scope.visibleComments = false;
              scope.visibleNeeds = true;
            };

            scope.toggleCommentSection = function () {
              scope.visibleNeeds = false;
              scope.comment = '';
            };

            scope.toggleComments = function () {
              scope.visibleNeeds = false;
              scope.visibleComments = !scope.visibleComments;
            };

            scope.saveNeed = function (keyCode) {
              if (keyCode === 13) {
                scope.visibleNeeds = false;
                scope.visibleComments = true;
                scope.showLoadingBar = true;

                var need = {
                  PublicationId: scope.publication.PublicationId,
                  UserId: scope.currentUserId,
                  Comment: scope.comment
                };

                needRestService.post(need).then(
                  function (response) {
                    scope.publication.Requesters.unshift(response);
                    scope.myNeeds.unshift(response);
                  }
                );
                scope.showLoadingBar = false;
              }
            };
            $rootScope.connection.start().done(
              function () {
                scope.givePublication = function (need) {
                  scope.publication.Given = true;
                  need.Given = true;
                  needRestService.one().customPUT(need, need.NeedId)
                    .then(function () {
                      $rootScope.hubConnnection.server.sendNotification(need.UserId);
                    })
                    .catch(function () {
                      scope.publication.Given = false;
                      need.Given = false;
                    });
                };
              }
            );
          }
        };
      });
})();