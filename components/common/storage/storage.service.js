﻿(function () {
  angular
    .module('common.storage')
    .factory('storageService', storageService);

  function storageService() {
    return {
      getItem: getItemFunction,
      setItem: setItemFunction,
      removeItem: removeItemFunction
    };
  }

  function getItemFunction(key, persist) {
    return getStorage(persist).getItem(key);
  }

  function setItemFunction(key, value, persist) {
    getStorage(persist).setItem(key, value);
  }

  function removeItemFunction(key, persist) {
    getStorage(persist).removeItem(key);
  }

  function getStorage(persist) {
    return persist ? localStorage : sessionStorage;
  }
})();