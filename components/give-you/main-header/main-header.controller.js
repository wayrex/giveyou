﻿(function () {
  'use strict';

  angular
    .module('giveYou.mainHeader')
    .controller('MainHeaderController', MainHeaderController);

  MainHeaderController.$inject = [
    '$rootScope',
    '$scope',
    '$state',
    'authService',
    'userInfoService',
    'notificationLoadService',
    'notificationRestService',
    'browserNotificationService'
  ];

  function MainHeaderController(
    $rootScope,
    $scope,
    $state,
    authService,
    userInfoService,
    notificationLoadService,
    notificationRestService,
    browserNotificationService) {
    var vm = this;
    var user = userInfoService.getUser();

    vm.fullUserName = user.FullName;
    vm.logout = authService.logout;
    vm.showOptions = false;
    vm.showNotifications = false;
    vm.notifications = $scope.notifications;

    vm.toggleOptions = function () {
      vm.showOptions = !vm.showOptions;
    };

    $rootScope.hubConnnection.client.reloadNotifications = function (id) {
      var userId = user.UserId;
      if (id == userId) {
        notificationLoadService.getNotifications(userId).then(function (response) {
          /*eslint-disable*/
          $scope.notifications = response;
          /*eslint-enable*/
          vm.notifications = response;
          vm.unreadNotificationsQuantity = getUnreadNotificationsCount();
          browserNotificationService.createNotification('You Have a new notification', response[0].Title);
          return response;
        },
        function (error) {
          return [];
        });
      }
    };

    vm.unreadNotificationsQuantity = getUnreadNotificationsCount();
    vm.notifications = $scope.notifications;

    vm.toggleNotifications = function () {
      vm.showNotifications = !vm.showNotifications;
    };

    function getUnreadNotificationsCount() {
      var unreadNotificationsCount = 0;
      $scope.notifications.forEach(function (notification) {
        if (!notification.Read) {
          ++unreadNotificationsCount;
        }
      });

      return unreadNotificationsCount;
    };

    vm.goToSelectedPublication = function (notification) {
      if (!notification.Read) {
        notification.Read = true;
        notification.customPUT();
        notificationRestService.one().customPUT(notification, notification.NotificationId)
          .then(function () {
            $state.go('main.publicationDetails', {
              publicationId: notification.LinkedObject.ObjectId
            });
          })
        .catch(function () {
        });
      } else {
        $state.go('main.publicationDetails', {
          publicationId: notification.LinkedObject.ObjectId
        });
      }
    };
  }
})();