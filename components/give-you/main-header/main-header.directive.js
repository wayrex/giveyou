﻿(function () {
  'use strict';
  angular.module('giveYou.mainHeader')
    .directive('mainHeader',
      function ($state, $document, $timeout) {
        return {
          restrict: 'E',
          scope: {
            notifications: '='
          },
          templateUrl: 'components/give-you/main-header/main-header.template.html',
          controller: 'MainHeaderController',
          controllerAs: 'vm',
          link: function (scope, element) {
            var listener = $document.on('click', function () {
              $timeout(function () {
                if (scope.vm.showOptions === true) {
                  scope.vm.toggleOptions();
                }
                if (scope.vm.showNotifications === true) {
                  scope.vm.toggleNotifications();
                }
              });
            });
            var optionsElement = angular.element(element[0].querySelector('#user-options'));
            optionsElement.on('click', function (event) {
              $timeout(function () {
                if (scope.vm.showNotifications === true) {
                  scope.vm.toggleNotifications();
                }
                scope.vm.toggleOptions();
              });
              event.stopPropagation();
            });

            var notificationsElement = angular.element(element[0].querySelector('#image-icon-color'));
            notificationsElement.on('click', function (event) {
              $timeout(function () {
                if (scope.vm.showOptions === true) {
                  scope.vm.toggleOptions();
                }
                scope.vm.toggleNotifications();
              });
              event.stopPropagation();
            });

            $document.on('$destroy', function () {
              listener();
            });
          }
        };
      });
})();