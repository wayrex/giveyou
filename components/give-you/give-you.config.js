﻿(function () {
  'use strict';

  angular
    .module('giveYou')
    .config(giveYouConfig);

  giveYouConfig.$inject = ['$stateProvider', 'RestangularProvider', 'BASE_URL'];

  function giveYouConfig($stateProvider, RestangularProvider, BASE_URL) {
    RestangularProvider.setBaseUrl(BASE_URL);
    $stateProvider.state('main', {
      url: ''
    });
  }
})();