﻿/* global $ */
(function () {
  'use strict';

  angular
    .module('giveYou')
    .run(giveYouRun);

  giveYouRun.$inject = ['$rootScope', '$state', '$timeout', '$location', 'authorizationService', 'SIGNALR_PATH'];

  function giveYouRun($rootScope, $state, $timeout, $location, authorizationService, SIGNALR_PATH) {
    $rootScope.connection = $.connection.hub;
    $rootScope.hubConnnection = $.connection.notificationHub;
    $.connection.hub.url = SIGNALR_PATH;
    var listener = $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {
      authorizationService.validateLogin(toState);
    });

    $rootScope.$on('$destroy', listener);
  }
})();