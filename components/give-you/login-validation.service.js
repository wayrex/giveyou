﻿(function () {
  'use strict';

  angular
    .module('giveYou')
    .factory('loginValidationService', loginValidationService);

  loginValidationService.$inject = ['$timeout', '$state', 'userInfoService'];

  function loginValidationService($timeout, $state, userInfoService) {
    var validateLogin = function (toState) {
      if (angular.isUndefined(toState.needsLogin)) {
        toState.needsLogin = true;
      }
      var user = userInfoService.getUser();
      if (user.UserID === null && toState.needsLogin !== false) {
        $timeout(function () {
          $state.transitionTo('login');
        });
      }
    };
    return validateLogin;
  }
})();