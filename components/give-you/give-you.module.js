﻿(function () {
  'use strict';

  angular
    .module('giveYou', [
      'common',
      'giveYou.publication',
      'giveYou.publicationDetails',
      'giveYou.login',
      'giveYou.mainHeader'
    ]);
})();