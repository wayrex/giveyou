﻿(function () {
  'use strict';

  angular
    .module('giveYou.publication', ['common.thirdPartyDependencies', 'common.services', 'common.userInfo']);
})();
