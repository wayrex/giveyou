﻿(function () {
  'use strict';

  angular
    .module('giveYou.publicationDetails', [
      'common.thirdPartyDependencies',
      'common.services',
    ]);
})();