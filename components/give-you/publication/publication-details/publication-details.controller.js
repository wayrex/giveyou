﻿(function () {
  'use strict';
  angular
    .module('giveYou.publicationDetails')
    .controller('PublicationDetailsController', PublicationDetailsController);
  PublicationDetailsController.$inject = [
    '$state',
    'publication',
    'scrollService',
    'RESOURCES_URL'
  ];

  function PublicationDetailsController(
    $state,
    publication,
    scrollService,
    RESOURCES_URL
  ) {
    var vm = this;

    publication.ImageURL = RESOURCES_URL + publication.ImageName;
    vm.publication = publication;
    scrollService.moveTo('publication-details');
    vm.goToFeed = function () {
      $state.go('main.publications');
    };
  }
})();
