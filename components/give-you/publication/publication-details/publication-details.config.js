﻿(function () {
  'use strict';

  angular
    .module('giveYou.publicationDetails')
    .config(publicationDetailsConfig);

  publicationDetailsConfig.$inject = [
    '$stateProvider',
    'RESOURCES_URL'
  ];

  function publicationDetailsConfig($stateProvider, RESOURCES_URL) {
    $stateProvider.state('main.publicationDetails', {
      url: '/publication/:publicationId',
      views: {
        'main-header@': {
          template: '<main-header notifications=notifications></main-header>',
          controller: function ($scope, notifications) {
            $scope.notifications = notifications;
          },
          resolve: {
            notifications: function (notificationLoadService, userInfoService) {
              var param = userInfoService.getUser().UserId;
              return notificationLoadService.getNotifications(param).then(function (response) {
                return response;
              },
              function (error) {
                return [];
              });
            }
          }
        },
        'content@': {
          templateUrl: 'components/give-you/publication/publication-details/publication-details.template.html',
          controller: 'PublicationDetailsController',
          controllerAs: 'vm',
          resolve: {
            publication: function (publicationRestService, $stateParams) {
              return publicationRestService.one('publication').get({PublicationId: $stateParams.publicationId});
            }
          }
        }
      }
    });
  }
})();