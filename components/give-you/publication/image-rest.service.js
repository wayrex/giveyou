﻿(function () {
  'use strict';

  angular
    .module('giveYou.publication')
    .factory('imageRestService', imageRestService);

  imageRestService.$inject = ['Restangular'];

  function imageRestService(Restangular) {
    return {
      postImage: postImage
    };
    function postImage(formData) {
      return Restangular.one('Images')
             .withHttpConfig({transformRequest: angular.identity})
             .customPOST(formData, '', undefined, {'Content-Type': undefined});
    }
  }
})();