﻿(function () {
  'use strict';
  angular
    .module('giveYou.publication')
    .controller('PublicationController', PublicationController);
  PublicationController.$inject = [
    '$state',
    '$scope',
    '$window',
    'publications',
    'myPublications',
    'myNeededPublications',
    'categories',
    'publicationRestService',
    'imageRestService',
    'userInfoService',
    'needRestService',
    'publicationLoadService',
    'scrollService',
    'storageService',
    'RESOURCES_URL'
  ];

  function PublicationController(
    $state,
    $scope,
    $window,
    publications,
    myPublications,
    myNeededPublications,
    categories,
    publicationRestService,
    imageRestService,
    userInfoService,
    needRestService,
    publicationLoadService,
    scrollService,
    storageService,
    RESOURCES_URL
  ) {
    var vm = this;
    vm.publications = publications;
    vm.myPublications = myPublications;
    vm.myNeededPublications = myNeededPublications;

    vm.allCategories = categories.map(function (item) {
      item.isChecked = true;
      return item;
    });
    vm.myINeedIsActive = false;
    vm.myPostsIsActive = false;
    vm.showLoadingBarInSection = false;

    vm.getCurrentUserId = function () {
      return parseInt(userInfoService.getUser().UserId, 10);
    };

    vm.savePublication = function (description, category, publicationType, image) {
      var user = userInfoService.getUser();
      var publication = {
        Description: description,
        CategoryId: category,
        UserDto: {
          UserId: user.UserId,
          FullName: user.FullName,
          EmailAddress: user.EmailAddress
        },
        PublicationType: publicationType,
        IsPublished: !image
      };
      return publicationRestService.post(publication)
        .then(function (response) {
          var formData = new FormData();
          formData.append('file', image);
          formData.append('publicationID', response.PublicationId);

          if (image) {
            imageRestService.postImage(formData)
              .then(function (responseImage) {
                var user = userInfoService.getUser();
                response.ImageURL = RESOURCES_URL + responseImage;
                response.UserName = user.FullName;
                setPublicationList(response);
              });
          } else {
            response.UserName = user.FullName;
            setPublicationList(response);
          }
          scrollService.moveTo('feed');
        });
    };

    function setPublicationList(response) {
      if (vm.myINeedIsActive) {
        vm.myPublications.unshift(response);
      } else if (vm.myPostsIsActive) {
        vm.myPublications.unshift(response);
        vm.publications = vm.myPublications;
      } else {
        vm.myPublications.unshift(response);
        vm.publications.unshift(response);
      }
    }

    vm.getAllPublications = function () {
      clearPublicationList();
      vm.myINeedIsActive = false;
      vm.myPostsIsActive = false;
      publicationLoadService.getPublications()
        .then(onGetPublicationsSuccess);
    };

    vm.getMyPublications = function () {
      clearPublicationList();
      vm.myINeedIsActive = false;
      vm.myPostsIsActive = true;
      var param = {
        'publicationCreatorUserId': vm.getCurrentUserId()
      };
      publicationLoadService.getPublications(param)
        .then(onGetPublicationsSuccess);
    };

    vm.getMyNeededPublications = function () {
      clearPublicationList();
      vm.myINeedIsActive = true;
      vm.myPostsIsActive = false;
      var parameters = {
        'publicationRequesterUserId': vm.getCurrentUserId()
      };
      publicationLoadService.getPublications(parameters)
        .then(onGetPublicationsSuccess)
        .catch(onError);
    };

    vm.filterByCategory = function (element) {
      var result = vm.allCategories.filter(function (category) {
        return category.CategoryId === element.CategoryId;
      });
      return result[0] ? result[0].isChecked : false;
    };

    vm.storageCategory = function (category) {
      userInfoService.storeCategoryInLocalStorage(category.CategoryId);
    };

    vm.verifyCategory = function (category) {
      category.isChecked = userInfoService.verifyExistsCategory(category.CategoryId);
    };

    function onGetPublicationsSuccess(result) {
      vm.publications = result;
      vm.showLoadingBarInSection = false;
    }

    function onError(error) {
      vm.publications = [];
      vm.showLoadingBarInSection = false;
    }

    function clearPublicationList() {
      vm.publications = [];
      scrollService.moveTo('feed');
      vm.showLoadingBarInSection = true;
    }
  }
})();
