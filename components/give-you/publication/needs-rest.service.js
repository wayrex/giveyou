﻿(function () {
  'use strict';

  angular
    .module('giveYou.publication')
    .factory('needsRestService', needsRestService);

  needsRestService.$inject = ['$q', '$timeout'];

  function needsRestService($q, $timeout) {
    var defer;
    defer = $q.defer();
    $timeout(function () {
      defer.resolve([
  {'UserID': '1', 'FullName': 'Wayra Loayza'},
  {'UserID': '2', 'FullName': 'Daniela Terrazas'},
  {'UserID': '3', 'FullName': 'Paolo Salazar'},
  {'UserID': '4', 'FullName': 'Rafael Encinas'},
  {'UserID': '5', 'FullName': 'Elvis Tec'}
      ]);
    }, 5000);
    return defer.promise;
  }
})();