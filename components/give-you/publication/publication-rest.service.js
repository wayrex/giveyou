﻿(function () {
  'use strict';

  angular
    .module('giveYou.publication')
    .factory('publicationRestService', publicationRestService);

  publicationRestService.$inject = ['Restangular'];

  function publicationRestService(Restangular) {
    return Restangular.service('Publication');
  }
})();