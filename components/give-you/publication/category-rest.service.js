﻿(function () {
  'use strict';

  angular
    .module('giveYou.publication')
    .factory('categoryRestService', categoryRestService);

  categoryRestService.$inject = ['Restangular'];

  function categoryRestService(Restangular) {
    return Restangular.service('Categories');
  }
})();