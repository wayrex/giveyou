﻿(function () {
  'use strict';

  angular
    .module('giveYou.publication')
    .factory('notificationRestService', notificationRestService);

  notificationRestService.$inject = ['Restangular'];

  function notificationRestService(Restangular) {
    return Restangular.service('notifications');
  }
})();