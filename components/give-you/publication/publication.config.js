﻿(function () {
  'use strict';

  angular
    .module('giveYou.publication')
    .config(publicationConfig);

  publicationConfig.$inject = [
    '$stateProvider',
    'RESOURCES_URL'
  ];

  function publicationConfig($stateProvider, RESOURCES_URL) {
    $stateProvider.state('main.publications', {
      url: '/feed',
      views: {
        'main-header@': {
          template: '<main-header notifications=notifications></main-header>',
          controller: function ($scope, notifications) {
            $scope.notifications = notifications;
          },
          resolve: {
            notifications: function (notificationLoadService, userInfoService) {
              var userId = userInfoService.getUser().UserId;
              return notificationLoadService.getNotifications(userId).then(function (response) {
                return response;
              },
              function (error) {
                return [];
              });
            }
          }
        },
        'content@': {
          templateUrl: 'components/give-you/publication/publication.template.html',
          resolve: {
            publications: function (publicationLoadService) {
              return publicationLoadService.getPublications();
            },
            myPublications: function (publications, userInfoService) {
              var userId = userInfoService.getUser().UserId ;
              var publicationsByUserId = [];
              publications.forEach(function (publication) {
                if (publication.UserDto.UserId == userId) {
                  publicationsByUserId.push(publication);
                }
              });
              return publicationsByUserId;
            },
            myNeededPublications: function (publicationLoadService, userInfoService) {
              var param = {
                'publicationRequesterUserId': userInfoService.getUser().UserId
              };
              return publicationLoadService.getPublications(param);
            },
            categories: function (categoryRestService) {
              return categoryRestService.getList();
            }
          },
          controller: 'PublicationController',
          controllerAs: 'vm'
        }
      }
    });
  }
})();
