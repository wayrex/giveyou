﻿(function () {
  'use strict';
  angular.module('giveYou.publication')
    .directive('createPublication',
      function ($state, $document, $timeout, publicationRestService, categoryRestService, MAX_SIZE_FILE_IN_BYTES) {
        return {
          restrict: 'E',
          scope: {
            message: '@',
            save: '&',
            categories: '=allCategories',
            visibleForm: '=expandedPublicationForm'
          },
          templateUrl: 'components/give-you/publication/create-publication/create-publication.template.html',
          link: function (scope, element) {
            scope.showForm = showForm;
            scope.hideForm = hideForm;
            scope.errorInCreatingPublication = false;
            scope.showLoadingBar = false;
            scope.publicationType = 'Offered';
            scope.selectedCategory = {};
            setInitialCategoryValue();

            scope.exceedMaxSizeFile = function () {
              if (scope.image) {
                return scope.image.size > MAX_SIZE_FILE_IN_BYTES;
              }
            };

            scope.savePublication = function () {
              scope.showLoadingBar = true;
              scope.errorInCreatingPublication = false;
              var savePromise = scope.save({
                description: scope.description,
                category: scope.selectedCategory.id,
                publicationType: scope.publicationType,
                image: scope.image
              });
              savePromise
                .then(function () {
                  scope.publicationForm.$setPristine();
                  scope.cleanForm();
                })
                .catch(function (errorMessage) {
                  scope.message = 'Something went wrong, please try again';
                  scope.showLoadingBar = false;
                  scope.errorInCreatingPublication = true;
                });
            };

            function setInitialCategoryValue() {
              scope.categories.forEach(function (category) {
                if (category.CategoryName === 'Others') {
                  scope.selectedCategory = {id: category.CategoryId};
                }
              });
            };

            function hideForm() {
              var descriptionText = angular.element(element[0].querySelector('#description')).val();

              if (descriptionText.length !== 0 || scope.image) {
                scope.visibleForm = true;
              } else {
                if (scope.publicationForm) {
                  scope.publicationForm.$setPristine();
                }
                scope.visibleForm = false;
              }
            }

            function showForm() {
              scope.visibleForm = true;
            }

            scope.cleanForm = function () {
              scope.description = '';
              scope.category = '';
              scope.image = null;
              scope.visibleForm = false;
              scope.showLoadingBar = false;
              scope.errorInCreatingPublication = false;
            };

            var listener = $document.on('click', function () {
              $timeout(function () {
                hideForm();
              });
            });

            element.on('click', function (event) {
              event.stopPropagation();
            });

            $document.on('$destroy', function () {
              listener();
            });

            scope.$watch('image', function (imageFile) {
              if (imageFile) {
                scope.visibleForm = true;
              } else {
                scope.visibleForm = scope.description ? true : false;
              }
            });
          }
        };
      });
})();