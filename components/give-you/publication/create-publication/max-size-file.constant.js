﻿(function () {
  'use strict';

  angular
    .module('giveYou.publication')
    .constant('MAX_SIZE_FILE_IN_BYTES', 3145728);
})();