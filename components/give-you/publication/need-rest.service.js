﻿(function () {
  'use strict';

  angular
    .module('giveYou.publication')
    .factory('needRestService', needRestService);

  needRestService.$inject = ['Restangular'];

  function needRestService(Restangular) {
    return Restangular.service('need');
  }
})();