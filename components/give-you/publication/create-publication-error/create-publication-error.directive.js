﻿(function () {
  'use strict';

  angular.module('giveYou.publication')
    .directive('createPublicationError',
      function ($state, $document, $timeout) {
        return {
          restrict: 'E',
          scope: {
            message: '@'
          },
          templateUrl: 'components/give-you/publication/create-publication-error/create-publication-error.template.html'
        };
      });
})();