﻿(function () {
  'use strict';

  angular.module('giveYou.publication')
    .directive('description',
      function ($state, $document, $timeout) {
        return {
          restrict: 'E',
          scope: {
            description: '@',
            username: '@',
            hasImage: '=',
            publication: '='
          },
          templateUrl: 'components/give-you/publication/description/description.template.html',
          link: function (scope, element) {
            scope.collapsed = scope.hasImage;
            scope.publicationEventype = ' is sharing';
            if (scope.publication.PublicationType) {
              scope.publicationEventype = (scope.publication.PublicationType === 'Offered') ? ' is sharing' : ' is looking for';
            }
            scope.toggle = function () {
              if (scope.hasImage) {
                scope.collapsed = !scope.collapsed;
              }
            };
          }
        };
      });
})();