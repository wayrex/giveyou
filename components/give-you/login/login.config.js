﻿(function () {
  'use strict';

  angular
    .module('giveYou.login')
    .config(loginConfig);

  loginConfig.$inject = ['$stateProvider', 'RESOURCES_URL'];

  function loginConfig($stateProvider, RESOURCES_URL) {
    $stateProvider.state('login', {
      url: '/login',
      needsLogin: false,
      views: {
        'content': {
          templateUrl: 'components/give-you/login/login.template.html',
          controller: 'LoginController',
          controllerAs: 'vm'
        }
      }
    });
  }
})();