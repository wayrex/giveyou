﻿(function () {
  'use strict';

  angular
    .module('giveYou.login')
    .controller('LoginController', LoginController);

  LoginController.$inject = [
    '$scope',
    'authService',
    '$state'
  ];

  function LoginController($scope, authService, $state) {
    var vm = this;
    vm.disabledFields = false;
    function loginFunction() {
      vm.disabledFields = true;
      authService.login(vm.username, vm.password).then(
        function (response) {
          $state.go('main.publications');
        },
        function (error) {
          vm.username = '';
          vm.password = '';
          vm.disabledFields = false;
        }
      );
    }

    vm.login = loginFunction;
  }
})();