// Karma configuration
// Generated on Fri Mar 04 2016 00:03:08 GMT-0400 (BOT)

module.exports = function (config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'assets/js/angular/angular.js',
      'assets/js/angular-mocks/angular-mocks.js',
      'assets/js/angular-resource/angular-resource.js',
      'assets/js/angular-timeago/dist/angular-timeago.js',
      'assets/js/angular-ui-router/release/angular-ui-router.js',
      'assets/js/lodash/lodash.js',
      'assets/js/restangular/dist/restangular.js',
      'assets/js/underscore/underscore.js',
      'components/common/third-party-dependencies.module.js',
      'components/common/common.module.js',
      'components/common/directive/file-loader.directive.js',
      'components/common/directive/loading-bar/loading-bar.template.html',
      'components/common/directive/loading-bar/loading-bar.directive.js',
      'components/common/directive/need/need.template.html',
      'components/common/directive/need/need.directive.js',
      'components/common/base-url.constant.js',
      'components/common/resources.constant.js',
      'components/common/authentication/authentication.module.js',
      'components/common/authentication/authentication.service.js',
      'components/common/authentication/authentication-rest.service.js',
      'components/common/user-info/user-info.module.js',
      'components/common/user-info/user-info.service.js',
      'components/common/storage/storage.module.js',
      'components/common/storage/storage.service.js',
      'components/common/services/services.module.js',
      'components/common/services/authorization.service.js',
      'components/common/services/publications-load.service.js',
      'components/common/services/notifications-load.service.js',
      'components/common/services/scroll.service.js',
      'components/common/services/browser-notification.service.js',
      'components/give-you/publication/publication.module.js',
      'components/give-you/publication/category-rest.service.js',
      'components/give-you/publication/publication-rest.service.js',
      'components/give-you/publication/notification-rest.service.js',
      'components/give-you/publication/need-rest.service.js',
      'components/give-you/publication/publication.controller.js',
      'components/give-you/publication/image-rest.service.js',
      'components/give-you/publication/description/description.template.html',
      'components/give-you/publication/description/description.directive.js',
      'components/give-you/give-you.module.js',
      'components/give-you/give-you.controller.js',
      'components/give-you/give-you.config.js',
      'components/give-you/publication/create-publication/create-publication.template.html',
      'components/give-you/publication/create-publication/create-publication.directive.js',
      'components/give-you/publication/create-publication/max-size-file.constant.js',
      'components/give-you/login/login.module.js',
      'components/give-you/login/login.controller.js',
      'components/give-you/main-header/main-header.module.js',
      'components/give-you/main-header/main-header.controller.js',
      'tests/**/*.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'components/**/*.controller.js': ['coverage'],
      'components/**/*.directive.js': ['coverage'],
      'components/**/*.service.js': ['coverage'],
      'components/**/*.filter.js': ['coverage'],
      'components/give-you/**/*.html': ['ng-html2js'],
      'components/common/directive/**/*.html': ['ng-html2js']
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'coverage'],

    // web server port
    port: 5000,

    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,

    coverageReporter: {
      type: 'html',
      dir: 'coverage/'
    }
  });
};