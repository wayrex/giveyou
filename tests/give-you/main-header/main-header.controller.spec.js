﻿describe('main-header Controller Test', function () {
  var scope = {};
  var userInfoService;
  var authService;
  var notifications;
  var notificationRestService;
  var notificationLoadService;
  var reloadNotificationsSuccess;
  var $state = {go: function () { }};

  beforeEach(angular.mock.module(function ($provide) {
    $provide.value('$state', $state);
  }));

  beforeEach(angular.mock.module('giveYou.mainHeader'));

  beforeEach(angular.mock.module('giveYou.login'));

  beforeEach(angular.mock.module(function (_$provide_) {
    _$provide_.factory('userInfoService', function (_$q_) {
      var getUser = jasmine.createSpy('getUser').and.callFake(function () {
        var userInfo = {FullName: 'Pepito', UserId: 123};
        return userInfo;
      });
      return {
        getUser: getUser
      };
    });

    _$provide_.factory('notifications', function () {
      var notificationInfo = [
        {
          'CreatedDateTime': '2016-03-15T08:30:00',
          'LinkedObject': {'ObjectId': 1, 'ObjectType': 'publication'},
          'NotificationId': 1,
          'Read': true,
          'Title': 'Pepito has accepted your \"I need it\"',
          'UserDto': {'UserId': 123, 'FullName': 'Pepito'}
        },
        {
          'CreatedDateTime': '2016-04-15T08:30:00',
          'LinkedObject': {'ObjectId': 2, 'ObjectType': 'publication'},
          'NotificationId': 2,
          'Read': false,
          'Title': 'Pepito has accepted your \"I need it\"',
          'UserDto': {'UserId': 123, 'FullName': 'Pepito'}
        }
      ];
      return notificationInfo;
    });

    _$provide_.factory('authService', function (_$q_) {
      var logout = jasmine.createSpy('logout').and.callFake(function () {
      });
      return {
        logout: logout
      };
    });

    _$provide_.factory('notificationLoadService', function (_$q_) {
      var getNotifications = jasmine.createSpy('getNotifications').and.callFake(function (notification) {
        var deferred = _$q_.defer();
        notification = {
          UserId: '1',
          Message: 'some message',
          forEach: function () { }
        };

        if (reloadNotificationsSuccess === true) {
          deferred.resolve(notification);
        } else {
          deferred.reject([]);
        }

        return deferred.promise;
      });

      return {
        getNotifications: getNotifications
      };
    });

    notificationRestService = _$provide_.factory('notificationRestService', function (_$q_) {
      var service = jasmine.createSpy('service');
      var one = function () {
        return {
          customPUT: function () {
            return {
              catch: function () { }
            };
          }
        };
      };
      return {
        service: service,
        one: one,
      };
    });

  }));

  beforeEach(angular.mock.inject(function (
    $controller,
    $rootScope,
    _userInfoService_,
    _authService_,
    _notifications_,
    _notificationRestService_,
    _$state_
    ) {
    scope = $rootScope.$new();
    $rootScope.hubConnnection = {
      client: {
        reloadNotifications: function () { }
      }
    };
    userInfoService = _userInfoService_;
    notifications = _notifications_;
    authService = _authService_;
    $controller('MainHeaderController as vm', {
      $scope: scope,
      userInfoService: _userInfoService_,
      authService: _authService_,
      $state: _$state_
    });
  }));

  it('should call userInfoService.getUser() when loading the controller', function () {
    expect(userInfoService.getUser).toHaveBeenCalled();
  });

  it('should set fullUserName with the user name', function () {
    expect(scope.vm.fullUserName).not.toBe(null);
  });

  it('should call authService.logout when logout method in controller is called', function () {
    scope.vm.logout();
    expect(authService.logout).toHaveBeenCalled();
  });

  it('should toggle the showOptions variable between false and true when toggleOptions method is called', function () {
    scope.vm.showOptions = false;

    scope.vm.toggleOptions();
    expect(scope.vm.showOptions).toBe(true);

    scope.vm.toggleOptions();
    expect(scope.vm.showOptions).toBe(false);
  });

  it('should call notification.customPUT when goToSelectedPublication  method in controller is called', function () {
    var notification = {
      'CreatedDateTime': '2016-03-15T08:30:00',
      'LinkedObject': {'ObjectId': 1, 'ObjectType': 'publication'},
      'NotificationId': 1,
      'Read': true,
      'Title': 'Pepito has accepted your \"I need it\"',
      'UserDto': {'UserId': 123, 'FullName': 'Pepito'},
      'customPUT': function () { }
    };
    spyOn(notification, 'customPUT');
    spyOn($state, 'go').and.callFake(function () { });

    scope.$digest();
    scope.vm.goToSelectedPublication(notification);
    expect(notification.customPUT).toHaveBeenCalled();
    expect($state.go).toHaveBeenCalled();
  });

  it('should have unreadNotifications count in one when one of two notifications is read', function () {
    expect(scope.vm.getUnreadNotificationsCount()).toEqual(1);
  });

  it('should call get notifications method', function () {
    reloadNotificationsSuccess = true;
    scope.hubConnnection.client.reloadNotifications();
    scope.$digest();
    expect(scope.vm.notifications).not.toBe(undefined);
  });


  it('should call get notifications methods', function () {
    reloadNotificationsSuccess = false;
    scope.hubConnnection.client.reloadNotifications();
    scope.$digest();
    expect(scope.vm.notifications).not.toBe(undefined);
  });
});