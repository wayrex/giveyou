﻿describe('Login Controller Test', function () {
  var scope = {};
  var authService;
  var $state = {go: function () { }};
  var promise = false;

  beforeEach(angular.mock.module(function ($provide) {
    $provide.value('$state', $state);
  }));

  beforeEach(angular.mock.module(function (_$provide_) {
    _$provide_.factory('authService', function (_$q_) {
      var login = jasmine.createSpy('login').and.callFake(function (username, password) {
        var deferred = _$q_.defer();

        if (promise === true) {
          deferred.resolve({});
        } else {
          deferred.reject({});
        }

        return deferred.promise;
      });
      return {
        login: login
      };
    });
  }));

  beforeEach(angular.mock.module('giveYou.login'));

  beforeEach(angular.mock.inject(function ($controller,
                                           $rootScope,
                                           _authService_) {
    scope = $rootScope.$new();
    authService = _authService_;
    $controller('LoginController as vm', {
      $scope: scope
    });
  }));

  it('should call state.go() when user login is success', function () {
    promise = true;
    scope.vm.login();
    spyOn($state, 'go').and.callFake(function () { });
    expect(authService.login).toHaveBeenCalled();
    scope.$digest();
    expect($state.go).toHaveBeenCalled();
  });

  it('should set username and password to empty when user login failed', function () {
    promise = false;
    scope.vm.login();
    expect(authService.login).toHaveBeenCalled();
    scope.$digest();
    expect(scope.vm.username).toEqual('');
    expect(scope.vm.password).toEqual('');
  });
});
