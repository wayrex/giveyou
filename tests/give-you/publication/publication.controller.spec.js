describe('Publications Controller Test', function () {

  var scope = {};
  var promise = false;
  var listOfCategories = [
    {Name: 'Electronics'},
    {Name: 'Computers'}
  ];
  var listOfPublications = [
    {Description: 'A computer'},
    {Description: 'A Game board'}
  ];
  var passPromise;
  var publicationRestService;
  var userInfoService;
  var needRestService;
  var publicationsList = [
    {
      'PublicationId': 1,
      'Description': 'Description 1.',
      'ImageName': 'image1.jpg',
      'CreationDateTime': '2016-03-15T08:30:00',
      'LastActivity': '2016-03-15T08:30:00',
      'UserName': 'Username 1',
      'CategoryId': 1,
      'UserId': 1,
      'NeedQuantity': 0
    },
    {
      'PublicationId': 2,
      'Description': 'Description 2',
      'ImageName': 'jostick1.jpg',
      'CreationDateTime': '2016-03-12T10:20:00',
      'LastActivity': '2016-03-12T10:20:00',
      'UserName': 'Username 1',
      'CategoryId': 1,
      'UserId': 2,
      'NeedQuantity': 0
    }];

  var listOfPublicationsByUserId = [
    {
      'PublicationId': 1,
      'Description': 'Description 1.',
      'ImageName': 'image1.jpg',
      'CreationDateTime': '2016-03-15T08:30:00',
      'LastActivity': '2016-03-15T08:30:00',
      'UserName': 'Username 1',
      'CategoryId': 1,
      'UserId': 1,
      'NeedQuantity': 0
    },
    {
      'PublicationId': 2,
      'Description': 'Description 2',
      'ImageName': 'jostick1.jpg',
      'CreationDateTime': '2016-03-12T10:20:00',
      'LastActivity': '2016-03-12T10:20:00',
      'UserName': 'Username 1',
      'CategoryId': 1,
      'UserId': 2,
      'NeedQuantity': 0
    }];

  var listOfNeedPublicationsByUserId = [
    {
      'PublicationId': 1,
      'Description': 'Description 1.',
      'ImageName': 'image1.jpg',
      'CreationDateTime': '2016-03-15T08:30:00',
      'LastActivity': '2016-03-15T08:30:00',
      'UserName': 'Username 1',
      'CategoryId': 1,
      'UserId': 1,
      'NeedQuantity': 0
    },
    {
      'PublicationId': 2,
      'Description': 'Description 2',
      'ImageName': 'jostick1.jpg',
      'CreationDateTime': '2016-03-12T10:20:00',
      'LastActivity': '2016-03-12T10:20:00',
      'UserName': 'Username 1',
      'CategoryId': 1,
      'UserId': 2,
      'NeedQuantity': 0
    }];

  var publicationLoadService;
  var storageService;

  beforeEach(angular.mock.module('giveYou.publication'));

  beforeEach(angular.mock.module(function (_$provide_) {
    _$provide_.factory('publicationRestService', function (_$q_) {
      var post = jasmine.createSpy('post').and.callFake(function (publication) {
        var deferred = _$q_.defer();
        var publication = publicationsList;
        deferred.resolve(publication);
        return deferred.promise;
      });
      return {
        post: post
      };
    });
  }));

  beforeEach(angular.mock.module(function (_$provide_) {
    _$provide_.factory('publicationLoadService', function (_$q_) {
      var getPublications = jasmine.createSpy('getPublications').and.callFake(function () {
        var deferred = _$q_.defer();

        if (promise === true) {
          deferred.resolve({});
        } else {
          deferred.reject([]);
        }

        return deferred.promise;
      });
      return {
        getPublications: getPublications
      };
    });
  }));

  beforeEach(angular.mock.module(function (_$provide_) {
    _$provide_.factory('needRestService', function (_$q_) {
      var post = jasmine.createSpy('post').and.callFake(function (publication) {
        var deferred = _$q_.defer();
        var need = {
          UserId: '1',
          PublicationId: '2'
        };
        deferred.resolve(need);
        return deferred.promise;
      });
      return {
        post: post
      };
    });
  }));

  beforeEach(angular.mock.module(function (_$provide_) {
    _$provide_.factory('userInfoService', function () {
      var user = jasmine.createSpy('getUser').and.callFake(function () {
        var usr = {
          UserId: '1',
          FullName: 'paolo salazar'
        };

        return usr;
      });

      var verifyExistsCategory = jasmine.createSpy('verifyExistsCategory').and.callFake(function () {
        return false;
      });

      return {
        getUser: user,
        verifyExistsCategory: verifyExistsCategory
      };
    });
  }));

  beforeEach(angular.mock.module(function (_$provide_) {
    _$provide_.factory('storageService', function () {
      var localStorage = jasmine.createSpy('getItem').and.callFake(function () {
        var categoryStorage = {
          CategoryId: 1,
          isChecked: true,
          CategoryName: 'Home'
        };

        return categoryStorage;
      });

      var removeItem = jasmine.createSpy('removeItem');

      return {
        getItem: localStorage,
        removeItem: removeItem
      };
    });
  }));

  beforeEach(angular.mock.inject(function ($controller,
                                           $rootScope,
                                           _publicationRestService_,
                                           _userInfoService_,
                                           _needRestService_,
                                           _publicationLoadService_,
                                           _storageService_) {
    scope = $rootScope.$new();
    publicationRestService = _publicationRestService_;
    userInfoService = _userInfoService_;
    needRestService = _needRestService_;
    publicationLoadService = _publicationLoadService_;
    storageService: _storageService_;
    $controller('PublicationController as vm', {
      $scope: scope,
      publications: listOfPublications,
      myPublications: listOfPublicationsByUserId,
      myNeededPublications: listOfNeedPublicationsByUserId,
      categories: listOfCategories,
      publicationRestService: _publicationRestService_,
      userInfoService: _userInfoService_,
      needRestService: _needRestService_,
      publicationLoadService: _publicationLoadService_,
      storageService: _storageService_,
      RESOURCES_URL: ''
    });
  }));

  it('should have an array of publication objects', function () {
    expect(scope.vm.publications).toEqual([
      {Description: 'A computer'},
      {Description: 'A Game board'}
    ]);
  });

  it('should have an array of category objects', function () {
    expect(scope.vm.allCategories).toEqual([
      {Name: 'Electronics', isChecked: true},
      {Name: 'Computers', isChecked: true}
    ]);
  });

  it('should call publicationRestService.post() when saving a new publication', function () {
    scope.vm.savePublication();
    expect(publicationRestService.post).toHaveBeenCalled();
  });

  it('should add a new publication to the publications list', function () {
    var oldPublicationsSize = scope.vm.publications.length;
    scope.vm.savePublication();
    scope.$digest();
    var newPublicationsSize = scope.vm.publications.length;
    expect(newPublicationsSize).toBe(oldPublicationsSize + 1);
  });

  it('should call publicationRestService.getPublications() when getting all publications', function () {
    scope.vm.getAllPublications();
    scope.$digest();
    expect(publicationLoadService.getPublications).toHaveBeenCalled();
    expect(scope.vm.myINeedIsActive).toBe(false);
  });

  it('should call publicationRestService.getPublications() when getting My publications', function () {
    scope.vm.getMyPublications();
    expect(publicationLoadService.getPublications).toHaveBeenCalled();
  });

  it('should call publicationRestService.getPublications() when getting My needed publications', function () {
    promise = true;
    scope.vm.getMyNeededPublications();
    scope.$digest();
    expect(publicationLoadService.getPublications).toHaveBeenCalled();
    expect(scope.vm.showLoadingBarInSection).toEqual(false);
  });

  it('should call publicationRestService.getPublications() when getting My needed publications failed', function () {
    promise = false;
    scope.vm.getMyNeededPublications();
    scope.$digest();
    expect(publicationLoadService.getPublications).toHaveBeenCalled();
    expect(scope.vm.publications).toEqual([]);
    expect(scope.vm.showLoadingBarInSection).toEqual(false);
  });

  it('should call vm.allCategories.filter() method when filter by category is applied', function () {
    var element = {
      CategoryId: 1
    };
    spyOn(scope.vm.allCategories, 'filter').and.returnValue([{CategoryId: 1}]);
    scope.vm.filterByCategory(element);
    scope.$digest();
    expect(scope.vm.allCategories.filter).toHaveBeenCalled();
  });

  it('should set category isChecked property to false when verifyCategory is called', function () {
    var category = {
      CategoryId: 1,
      isChecked: true,
      CategoryName: 'Home'
    };
    scope.vm.verifyCategory(category);
    scope.$digest();
    expect(category.isChecked).toBe(false);
  });
});
