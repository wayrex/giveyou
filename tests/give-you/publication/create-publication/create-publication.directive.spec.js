describe('Description Directive Test', function () {

  var $rootScope;
  var $compile;
  var $q;
  var scope;
  var element;
  var isolatedScope;
  var template = [
    '<create-publication all-categories="categories" expanded-publication-form="visibleForm">',
    '</create-publication>'
  ].join(' ');

  function compileAndRender(scope, htmlCode) {
    var element = angular.element(htmlCode);
    $compile(element)(scope);
    scope.$digest();
    return element;
  }

  beforeEach(angular.mock.module(
    'components/give-you/publication/create-publication/create-publication.template.html'));

  beforeEach(angular.mock.module(
    'components/common/directive/loading-bar/loading-bar.template.html'));

  beforeEach(angular.mock.module('giveYou.publication'));

  beforeEach(angular.mock.inject(function (_$q_, _$compile_, _$rootScope_) {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    $q = _$q_;
  }));

  beforeEach(function () {
    scope = $rootScope.$new();
    scope.categories = [];
    scope.visibleForm = false;
    element = compileAndRender(scope, template);
    isolatedScope = element.isolateScope();
    isolatedScope.description = 'Description';
    isolatedScope.category = 'Category';
  });

  it('should replace the directive "create-publication" with the "create publication" template', function () {
    expect(element[0].querySelector('div')).not.toBeNull();
  });

  it('should have visibleForm variable set to false when directive is applied', function () {
    expect(isolatedScope.visibleForm).toBe(false);
  });

  it('should change visibleForm variable to true on showForm method call' +
    ' (with visibleForm previously set to true)', function () {
    isolatedScope.visibleForm = true;
    isolatedScope.showForm();
    expect(isolatedScope.visibleForm).toBe(true);
  });

  it('should change visibleForm variable to true on showForm method call' +
    ' (with visibleForm previously set to false)', function () {
    isolatedScope.visibleForm = false;
    isolatedScope.showForm();
    expect(isolatedScope.visibleForm).toBe(true);
  });

  it('should change visibleForm variable to false on hideForm method call' +
    ' (with visibleForm previously set to true)', function () {
    isolatedScope.visibleForm = true;
    isolatedScope.hideForm();
    expect(isolatedScope.visibleForm).toBe(false);
  });

  it('should leave visibleForm variable unchanged (false) on hideForm method call' +
    ' (with visibleForm set to false)', function () {
    isolatedScope.hideForm();
    expect(isolatedScope.visibleForm).toBe(false);
  });

  it('should set description and category to empty string on savePublication' +
    ' method call with successfull save', function () {
    var deferred = $q.defer();
    deferred.resolve([]);
    spyOn(isolatedScope, 'save').and.returnValue(deferred.promise);
    expect(isolatedScope.description).toMatch('Description');
    expect(isolatedScope.category).toMatch('Category');
    isolatedScope.savePublication();
    scope.$digest();
    expect(isolatedScope.description).toMatch('');
    expect(isolatedScope.category).toMatch('');
  });

  it('should leave description and category fields untouched on savePublication' +
    ' method call with failed save', function () {
    var deferred = $q.defer();
    deferred.reject([]);
    var expectedDescription = isolatedScope.description;
    var expectedCategory = isolatedScope.category;
    spyOn(isolatedScope, 'save').and.returnValue(deferred.promise);
    expect(isolatedScope.description).toMatch('Description');
    expect(isolatedScope.category).toMatch('Category');
    isolatedScope.savePublication();
    scope.$digest();
    expect(isolatedScope.description).toMatch(expectedDescription);
    expect(isolatedScope.category).toMatch(expectedCategory);
  });

  it('should set visibleForm to false (hide the form) on savePublication method call', function () {
    var deferred = $q.defer();
    spyOn(isolatedScope, 'save').and.returnValue(deferred.promise);
    spyOn(isolatedScope, 'cleanForm');
    isolatedScope.savePublication();
    expect(isolatedScope.visibleForm).toBe(false);

  });

  it('should set forms description and category as empty spaces and image as null on cleanForm method call',
    function () {
      expect(isolatedScope.description).toMatch('Description');
      expect(isolatedScope.category).toMatch('Category');
      isolatedScope.image = {Object: 'this is not null'};
      isolatedScope.cleanForm();
      expect(isolatedScope.description).toMatch('');
      expect(isolatedScope.category).toMatch('');
      expect(isolatedScope.image).toBeNull();
    });

  it('should set visibleForm, showLoadingBar and errorInCreatingPublication flags to false on cleanFormmethod call',
    function () {
      isolatedScope.visibleForm = true;
      isolatedScope.showLoadingBar = true;
      isolatedScope.selectedCategory = {id: 4};
      isolatedScope.errorInCreatingPublication = true;
      isolatedScope.cleanForm();
      expect(isolatedScope.visibleForm).toBe(false);
      expect(isolatedScope.showLoadingBar).toBe(false);
      expect(isolatedScope.errorInCreatingPublication).toBe(false);
    });

});
