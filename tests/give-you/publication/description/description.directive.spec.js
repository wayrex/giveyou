﻿describe('Description Directive Test', function () {

  var $rootScope;
  var $compile;
  var scope;
  var element;
  var isolatedScope;
  var template = [
    '<description has-image = "hasImage" description = "some description" username = "Juan Perez Garcia">',
    '</description>'
  ].join(' ');

  function compileAndRender(scope, htmlCode) {
    var element = angular.element(htmlCode);
    $compile(element)(scope);
    scope.$digest();
    return element;
  }

  beforeEach(angular.mock.module(
    'components/give-you/publication/description/description.template.html'));
  beforeEach(angular.mock.module('giveYou.publication'));
  beforeEach(angular.mock.inject(function (_$compile_, _$rootScope_) {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

  }));

  beforeEach(function () {
    scope = $rootScope.$new();
    scope.hasImage = true;
    element = compileAndRender(scope, template);
    isolatedScope = element.isolateScope();
  });


  it('should replace the directive "description" with the "description" template', function () {
    scope = $rootScope.$new();
    element = compileAndRender(scope, template);
    expect(element[0].querySelector('div')).not.toBeNull();
  });


  it('should have "collapsed" variable set to true when directive is applied', function () {
    expect(isolatedScope.collapsed).toBe(true);
  });

  it('should change "collapsed" variable to false on toggle method call' +
    ' (with collapsed previously set to true)', function () {
    isolatedScope.collapsed = true;
    isolatedScope.toggle();
    expect(isolatedScope.collapsed).toBe(false);
  });

  it('should change "collapsed" variable to true on toggle method call' +
    ' (with visibleForm previously set to false)', function () {
    isolatedScope.collapsed = false;
    isolatedScope.toggle();
    expect(isolatedScope.collapsed).toBe(true);
  });

  it('should have "scope.description" when directive is applied', function () {
    expect(isolatedScope.description).toEqual('some description');
  });

  it('should have "scope.username" when directive is applied', function () {
    expect(isolatedScope.username).toEqual('Juan Perez Garcia');
  });
});
