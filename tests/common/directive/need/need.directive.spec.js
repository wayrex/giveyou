﻿describe('Need Directive Test', function () {
  var $rootScope;
  var $compile;
  var scope;
  var element;
  var isolatedScope;
  var needRestService;
  var mockTimeAgoFilter;
  var succesfulRequest;
  var need;

  var template = [
    '<need publication="publication" my-needs="myNeeds">',
    '</need>'
  ].join(' ');

  function compileAndRender(scope, htmlCode) {
    var element = angular.element(htmlCode);
    $compile(element)(scope);
    scope.$digest();
    return element;
  }
  /*eslint-disable*/
  beforeEach(function () {
    module(function ($provide) {
      $provide.value('timeAgoFilter', mockTimeAgoFilter);
    });

    mockTimeAgoFilter = function (value) {
      return value;
    };
  });
  /*eslint-enable*/
  beforeEach(angular.mock.module(function (_$provide_) {
    succesfulRequest = true;
    _$provide_.factory('needRestService', function (_$q_) {
      var post = jasmine.createSpy('post').and.callFake(function (publication) {
        var deferred = _$q_.defer();
        var need = {
          UserId: '1',
          PublicationId: '2',
          Comment: 'I want this'
        };
        deferred.resolve(need);
        return deferred.promise;
      });
      var one = jasmine.createSpy('one').and.callFake(function () {
        return {
          customPUT: function (need) {
            var deferred = _$q_.defer();
            if (succesfulRequest === true) {
              deferred.resolve();
            } else {
              deferred.reject();
            }
            return deferred.promise;
          }
        };
      });
      return {
        post: post,
        one: one
      };
    });
  }));

  beforeEach(function () {
    need = {
      'NeedId': 1007,
      'UserId': 1002,
      'PublicationId': 2,
      'UserName': 'Dani',
      'Comment': 'bcgbbdfgvfdbvdf',
      'CreationDateTime': '2016-04-27T09:54:44.397',
      'Given': true
    };
  });

  beforeEach(angular.mock.module(
    'components/common/directive/need/need.template.html'));

  beforeEach(angular.mock.module(
    'components/common/directive/loading-bar/loading-bar.template.html'));

  beforeEach(angular.mock.module('common'));
  beforeEach(angular.mock.inject(function (_$compile_, _$rootScope_, _needRestService_) {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    needRestService = _needRestService_;
  }));

  beforeEach(function () {
    scope = $rootScope.$new();
    $rootScope.connection = {
      start: function () {
        return {
          done: function () { }
        };
      }
    };
    scope.publication = {
      UserDto: {
        UserId: 1
      },
      Requesters: [
        {
          PublicationId: 1,
          UserId: 2,
          Comment: 'comment'
        }
      ]
    };
    scope.myNeeds = [];
    element = compileAndRender(scope, template);
    isolatedScope = element.isolateScope();
  });

  it('should replace the directive "need" with the "need" template', function () {
    scope = $rootScope.$new();
    scope.publication = {
      UserDto: {
        UserId: 1
      },
      Requesters: []
    };
    scope.myNeeds = [];
    element = compileAndRender(scope, template);
    scope.$digest();
    expect(element[0].querySelector('div')).not.toBeNull();
  });

  it('should have "visibleComments" variable set to false when directive is applied', function () {
    expect(isolatedScope.visibleComments).toBe(false);
  });

  it('should return true to show Needs button', function () {
    scope.$digest();
    expect(isolatedScope.showNeedButton()).toBe(true);
  });

  it('should change "visibleComments" variable to false on expandNeedInput method call' +
  ' (with visibleComments previously set to true)', function () {
    isolatedScope.visibleComments = true;
    isolatedScope.expandNeedInput();
    expect(isolatedScope.visibleComments).toBe(false);
    expect(isolatedScope.visibleNeeds).toBe(true);
  });

  it('should change "visibleNeeds" variable to false on toggleCommentSection method call' +
  ' (with visibleNeeds previously set to true)', function () {
    isolatedScope.visibleNeeds = true;
    isolatedScope.toggleCommentSection();
    expect(isolatedScope.visibleNeeds).toBe(false);
    expect(isolatedScope.comment).toBe('');
  });

  it('should change "visibleNeeds" variable to false on toggleComments method call' +
  ' (with visibleNeeds previously set to true)', function () {
    isolatedScope.visibleNeeds = true;
    isolatedScope.visibleComments = true;
    isolatedScope.toggleComments();
    expect(isolatedScope.visibleNeeds).toBe(false);
    expect(isolatedScope.visibleComments).toBe(false);
  });

  it('should change "visibleNeeds" variable to false on saveNeed method call' +
  ' (with visibleNeeds previously set to true)', function () {
    spyOn(isolatedScope.publication.Requesters, 'unshift');
    spyOn(isolatedScope.myNeeds, 'unshift');
    isolatedScope.saveNeed(13);
    scope.$digest();
    expect(isolatedScope.visibleNeeds).toBe(false);
    expect(needRestService.post).toHaveBeenCalled();
    expect(isolatedScope.publication.Requesters.unshift).toHaveBeenCalled();
    expect(isolatedScope.myNeeds.unshift).toHaveBeenCalled();
  });
});


