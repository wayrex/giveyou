describe('Authentication Service Test', function () {
  var AuthenticationRestService;
  var userInfoService;
  var authService;
  var $state = {go: function (route) { }};

  beforeEach(function () {
    angular.mock.module(function (_$provide_) {
      _$provide_.value('$state', $state);
    });
    angular.mock.module('common.authentication');
  });

  beforeEach(angular.mock.module(function (_$provide_) {
    _$provide_.factory('userInfoService', function (_$q_) {
      var removeUser = jasmine.createSpy('removeUser');
      var saveUser = jasmine.createSpy('saveUser');
      return {
        removeUser: removeUser,
        saveUser: saveUser
      };
    });
  }));

  beforeEach(angular.mock.module(function (_$provide_) {
    AuthenticationRestService = _$provide_.factory('AuthenticationRestService', function (_$q_) {
      var post = jasmine.createSpy('post').and.callFake(function (credentials) {
        var deferred = _$q_.defer();
        deferred.resolve(function () { spyOn(userInfoService, 'saveUser');});
        return deferred.promise;
      });
      return {
        post: post
      };
    });
  }));

  /*eslint-disable*/
  beforeEach(inject(function (_AuthenticationRestService_, _userInfoService_, _authService_) {
    AuthenticationRestService = _AuthenticationRestService_;
    userInfoService = _userInfoService_;
    authService = _authService_;
  }));
  /*eslint-enable*/

  it('should call post method of AuthenticationRestService when login function is called', function () {
    authService.login();
    expect(AuthenticationRestService.post).toHaveBeenCalled();
  });

  it('should call removeUser method of userInfoService', function () {
    authService.logout();
    expect(userInfoService.removeUser).toHaveBeenCalled();
  });
});