﻿describe('Notification Load Service Test', function () {
  var Restangular;
  var notificationLoadService;
  var RESOURCES_URL = 'url';

  beforeEach(function () {
    angular.mock.module(function (_$provide_) {
      _$provide_.value('RESOURCES_URL', RESOURCES_URL);
    });
    angular.mock.module('common.services');
  });

  beforeEach(function () {
    angular.mock.module(function ($provide) {
      /*eslint-disable*/
      $provide.service('Restangular', function (_$q_) {
        this.one = jasmine.createSpy('post').and.callFake(function () {
          var getList = function () {
            return {
              then: function () { }
            };
          };
          return {
            getList:getList
          };
        });
      });
    });
  });

  beforeEach(inject(function (_Restangular_, _notificationLoadService_, _RESOURCES_URL_) {
    Restangular = _Restangular_;
    notificationLoadService = _notificationLoadService_;
    RESOURCES_URL = _RESOURCES_URL_;
  }));
  /*eslint-enable*/

  it('should call one method of Restangular when getNotifications method is called', function () {
    notificationLoadService.getNotifications();
    expect(Restangular.one).toHaveBeenCalled();
  });

});