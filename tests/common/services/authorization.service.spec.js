describe('Authorization Service Test', function () {
  var authorizationService;
  var AuthenticationRestService;
  var userInfoService;
  var $state = {go: function (route) { }};

  beforeEach(function () {
    angular.mock.module(function (_$provide_) {
      _$provide_.value('$state', $state);
    });
    angular.mock.module('common.services');
  });

  beforeEach(angular.mock.module(function (_$provide_) {
    _$provide_.factory('userInfoService', function (_$q_) {
      var getUser = jasmine.createSpy('getUser').and.callFake(function () {
        var user = {
          UserId: 1
        };

        return user;
      });
      return {
        getUser: getUser
      };
    });
  }));

  /*eslint-disable*/
  beforeEach(inject(function (_userInfoService_, _authorizationService_) {
    userInfoService = _userInfoService_;
    authorizationService = _authorizationService_;
  }));
  /*eslint-enable*/

  it('should call getUser method of UserInfoService', function () {
    var toState = {needsLogin: function () { }};
    authorizationService.validateLogin(toState);
    expect(userInfoService.getUser).toHaveBeenCalled();
  });
});