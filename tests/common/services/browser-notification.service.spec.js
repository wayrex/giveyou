﻿describe('Browser notifications test', function () {
  var browserNotificationService;
  var $window;

  beforeEach(function () {
    angular.mock.module('common.services');
  });

  /*eslint-disable*/
  beforeEach(inject(function (_$window_, _browserNotificationService_) {
    $window = _$window_;
    browserNotificationService = _browserNotificationService_;
  }));
  /*eslint-enable*/
  it('should return true if the method is called without parameters', function () {
    var result = browserNotificationService.createNotification();
    expect(result).toBe(true);
  });

  it('should return true if the createNotification method is called with parameters', function () {
    var result = browserNotificationService.createNotification('Title', 'Body');
    expect(result).toBe(true);
  });

  it('should return true if the permission is "default"', function () {
    var result = browserNotificationService.createNotification();
    expect(result).toBe(true);
  });

  it('should return false if the notification permission is "denied"', function () {
    $window.Notification = {
      permission: 'denied'
    };
    var result = browserNotificationService.createNotification();
    expect(result).toBe(false);
  });

  it('should return false if the Browser does not support Notifications', function () {
    $window.Notification = undefined;
    var result = browserNotificationService.createNotification();
    expect(result).toBe(false);
  });


});