describe('Publication Load Service Test', function () {
  var publicationLoadService;
  var publicationRestService;
  var RESOURCES_URL = 'url';

  beforeEach(function () {
    angular.mock.module(function (_$provide_) {
      _$provide_.value('RESOURCES_URL', RESOURCES_URL);
    });
    angular.mock.module('common.services');
  });

  beforeEach(angular.mock.module(function (_$provide_) {
    _$provide_.factory('publicationRestService', function (_$q_) {
      var getList = jasmine.createSpy('getList').and.callFake(function (credentials) {
        var deferred = _$q_.defer();
        deferred.resolve([]);
        return deferred.promise;
      });
      return {
        getList: getList
      };
    });
  }));

  /*eslint-disable*/
  beforeEach(inject(function (_publicationRestService_, _publicationLoadService_, _RESOURCES_URL_) {
    publicationRestService = _publicationRestService_;
    publicationLoadService = _publicationLoadService_;
    RESOURCES_URL = _RESOURCES_URL_;
  }));
  /*eslint-enable*/

  it('should call getList method of PublicationRestService', function () {
    publicationLoadService.getPublications();
    expect(publicationRestService.getList).toHaveBeenCalled();
  });
});