﻿describe('Storage Service Test', function () {
  var storageService;

  beforeEach(function () {
    angular.mock.module('common.storage');
  });

  /*eslint-disable*/
  beforeEach(inject(function (_storageService_) {
    storageService = _storageService_;
  }));
  /*eslint-enable*/

  it('should call getList method of PublicationRestService', function () {
    storageService.getItem('Identifier', false);
    expect(storageService.getItem).toBeDefined();
  });

  it('should call getList method of PublicationRestService', function () {
    storageService.setItem('Identifier', 1, false);
    expect(storageService.setItem).toBeDefined();
  });

  it('should call getList method of PublicationRestService', function () {
    storageService.removeItem('Identifier', false);
    expect(storageService.removeItem).toBeDefined();
  });

});