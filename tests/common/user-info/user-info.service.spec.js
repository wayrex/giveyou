describe('UserInfo Service Test', function () {
  var userInfoService;
  var storageService;

  beforeEach(function () {
    angular.mock.module('common.userInfo');
  });

  beforeEach(angular.mock.module(function (_$provide_) {
    _$provide_.factory('storageService', function (_$q_) {
      var setItem = jasmine.createSpy('setItem');
      var removeItem = jasmine.createSpy('removeItem');
      var getItem = jasmine.createSpy('createSpy').and.callFake(function () {
        var user = {
          UserId: 1,
          userPosition: [1],
          push: function () { }
        };
        return user;
      });
      return {
        setItem: setItem,
        removeItem: removeItem,
        getItem: getItem
      };
    });
  }));

  /*eslint-disable*/
  beforeEach(inject(function (_storageService_, _userInfoService_) {
    storageService = _storageService_;
    userInfoService = _userInfoService_;
  }));
  /*eslint-enable*/

  it('should call setItem method of StorageService', function () {
    var user = {
      UserId: 1,
      FullName: 'Paolo Salazar Villarroel',
      EmailAddress: 'paolo.salazar@fundacion-jala.org'
    };
    userInfoService.saveUser(user);
    expect(storageService.setItem).toHaveBeenCalled();
  });

  it('should call removeItem method of StorageService', function () {
    userInfoService.removeUser();
    expect(storageService.removeItem).toHaveBeenCalled();
  });

  it('should call verifyExistCategory method of StorageService', function () {
    var result = userInfoService.verifyExistsCategory();
    expect(result).toBe(true);
  });

  it('should call storeCategoryInLocalStorage method of StorageService', function () {
    userInfoService.storeCategoryInLocalStorage();
    expect(userInfoService.storeCategoryInLocalStorage).toBeDefined();
  });
});