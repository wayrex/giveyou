module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);
  grunt.initConfig({
    copy: {
      toDist: {
        expand: true,
        cwd: '',
        src: [
          '**/*.html',
          '**/assets/img/*.*',
          '**/assets/css/fonts/*.*',
          '!node_modules/**/*',
          '!dist/**/*',
          '!index.tpl.html',
          '!index.html'
        ],
        dest: 'dist/'
      },
      generated: {
        src: 'index.html',
        dest: 'dist/index.html'
      },
      renameIndex: {
        files: {
          'index.html': 'index.tpl.html'
        }
      },
      toDev: {
        src: [
          'assets/img/**/*',
          'assets/css/fonts/**/*',
          '!node_modules/**/*',
          '!dist/**/*',
          '!index.tpl.html',
          '!index.html'
        ],
        dest: '.tmp/'
      }
    },
    useminPrepare: {
      html: 'index.html',
      options: {
        dest: 'dist'
      }
    },
    usemin: {
      html: 'dist/index.html'
    },
    connect: {
      development: {
        options: {
          port: 3000,
          base: '',
          directory: null,
          open: true,
          protocol: 'https',
          key: grunt.file.read('cert/server.key').toString(),
          cert: grunt.file.read('cert/server.crt').toString(),
          ca: grunt.file.read('cert/ca.crt').toString(),
          keepalive: true
        }
      }
    },
    wiredep: {
      task: {
        src: ['index.html']
      }
    },
    /*eslint-disable*/
    angularFileLoader: {
      options: {
        scripts: ['components/**/*.js']
      },
      your_target: {
        src: ['index.html']
      }
    },
    injector: {
      options: {
        ignorePath: 'src/'
      },
      local_dependencies: {		
        files: {
          expand: true,
          'index.html': ['.tmp/reset.css', '.tmp/**/*.css']
        }
      }
    },
    /*eslint-enable*/
    eslint: {
      target: [
        '**/*.js',
        '!assets/**/*.js',
        '!node_modules/**/*.js',
        '!dist/**/*.js',
        '!coverage/**/*.js'
      ]
    },
    processhtml: {
      build: {
        files: {
          'dist/index.html': ['index.tpl.html']
        }
      }
    },
    includeSource: {
      options: {
        basePath: '',
        baseUrl: ''
      },
      myTarget: {
        files: {
          'index.html': 'index.tpl.html'
        }
      }
    },
    less: {
      dist: {
        options: {
          compress: true
        },
        files: {
          'dist/app.css': ['assets/css/reset.less', 'assets/css/**/*.less']
        }
      },
      dev: {
        files: [{
          expand: true,
          flatten: true,
          src: ['assets/css/**/*.less'],
          dest: '.tmp/',
          ext: '.css'
        }]
      }
    },
    karma: {
      unit: {
        configFile: 'karma.conf.js'
      }
    },
    clean: {
      development: {
        src: ['.tmp/']
      }
    },
    uglify: {
      options: {
        mangle: false
      }
    }
  });
  grunt.registerTask(
    'distribution', [
      'karma',
      'eslint',
      'copy:renameIndex',
      'angularFileLoader',
      'wiredep',
      'less:dist',
      'injector',
      'copy:toDist',
      'copy:generated',
      'useminPrepare',
      'concat',
      'uglify',
      'usemin',
      'processhtml'
    ]
  );
  grunt.registerTask(
    'development',
    [
      'newer:karma',
      'newer:eslint',
      'clean',
      'copy:renameIndex',
      'angularFileLoader',
      'wiredep',
      'newer:less:dev',
      'injector',
      'copy:toDev',
      'connect:development'
    ]
  );
};